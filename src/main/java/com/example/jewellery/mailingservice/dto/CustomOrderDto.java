package com.example.jewellery.mailingservice.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CustomOrderDto {

  private String message;
  private MultipartFile[] files;
}
