package com.example.jewellery.mailingservice.dto;

import lombok.Data;

@Data
public class ContactUsDto {

  private String name;
  private String message;
  private String fromEmail;
  private String subject;
}
