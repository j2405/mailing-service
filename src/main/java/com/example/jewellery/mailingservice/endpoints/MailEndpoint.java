package com.example.jewellery.mailingservice.endpoints;


import com.example.jewellery.mailingservice.clients.OrderServiceClient;
import com.example.jewellery.mailingservice.dto.ContactUsDto;
import com.example.jewellery.mailingservice.services.EmailSenderService;
import java.util.Arrays;
import javax.mail.MessagingException;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/mail")
@AllArgsConstructor
public class MailEndpoint {

  private EmailSenderService emailSenderService;
  private OrderServiceClient orderServiceClient;

  @PostMapping("/contact")
  public ResponseEntity<Void> sendContactUsEmail(@RequestBody ContactUsDto contactUsDto) {
    emailSenderService.sendEmail(contactUsDto.getFromEmail(), contactUsDto.getMessage(),
        contactUsDto.getName() + " - " + contactUsDto.getSubject());

    return ResponseEntity.ok().build();
  }

  @PostMapping("/order")
  public ResponseEntity<Void> sendOrderCreatedMail(@RequestBody String orderNumber) {
    //TODO take email from token
    emailSenderService.sendEmail("batantsi1980@gmail.com", "Order: " + orderNumber
            + " successfully received! We will contact you as soon as possible for confirmation of your order!\n "
            + "Thank you for being our client!\nBest Regards,\nAbsolute creations team",
        "Order: " + orderNumber);

    return ResponseEntity.ok().build();
  }

  @PostMapping(value = "/custom", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public ResponseEntity<Void> sendCustomOrderMail(@RequestParam String message,
      @RequestParam(required = false) MultipartFile[] files)
      throws MessagingException {
    //TODO take email from token
    var orderNumber = orderServiceClient.createCustomOrder(message);
    emailSenderService.sendCustomOrderMail("batantsi1980@gmail.com", message,
        "Custom order - " + orderNumber, files != null ? Arrays.asList(files) : null);

    return ResponseEntity.ok().build();
  }

  @GetMapping("/test")
  public String test() {

    return "test";
  }
}
