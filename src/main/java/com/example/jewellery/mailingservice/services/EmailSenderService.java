package com.example.jewellery.mailingservice.services;

import java.io.IOException;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.util.ByteArrayDataSource;
import lombok.AllArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@AllArgsConstructor
public class EmailSenderService {

  public static final String CONTACT_US_MAIL = "batantsi1980@gmail.com";

  private JavaMailSender javaMailSender;

  public void sendEmail(String fromEmail, String body, String subject) {
    SimpleMailMessage mail = new SimpleMailMessage();
    mail.setTo(CONTACT_US_MAIL);
    mail.setFrom(fromEmail);
    mail.setSubject(subject);
    mail.setText(body);

    javaMailSender.send(mail);
  }

  public void sendCustomOrderMail(String fromEmail, String body, String subject,
      List<MultipartFile> files)
      throws MessagingException {
    var mimeMessage = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

    helper.setTo(CONTACT_US_MAIL);
    helper.setFrom(fromEmail);
    helper.setSubject(subject);
    helper.setText(body);

    if (files != null) {
      files.forEach(file -> {
        try {
          helper.addAttachment(file.getOriginalFilename(),
              new ByteArrayDataSource(file.getBytes(), file.getContentType()));
        } catch (IOException | MessagingException e) {
          e.printStackTrace();
        }
      });
    }

    javaMailSender.send(mimeMessage);
  }
}
