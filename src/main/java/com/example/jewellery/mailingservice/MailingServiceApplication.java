package com.example.jewellery.mailingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MailingServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(MailingServiceApplication.class, args);
  }

}
