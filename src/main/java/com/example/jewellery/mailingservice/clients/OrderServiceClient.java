package com.example.jewellery.mailingservice.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "order-service")
public interface OrderServiceClient {

  @RequestMapping(method = RequestMethod.POST, value = "/orders/custom")
  String createCustomOrder(@RequestBody String description);
}
